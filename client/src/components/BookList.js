import React, { Component, Fragment } from 'react';
import { graphql } from 'react-apollo';

import BookDetails from './BookDetails';
import { getBooksQuery } from '../queries/queries';

class BookList extends Component {    
    constructor(props){
        super(props);
        this.state = {
            selected: null,
        }
    }

    render () {
        const { data } = this.props;
        
        return (
            <div>
                <ul id="book-list">
                {data.loading ? 
                    (
                        <div>
                            Loading books...
                        </div>
                    ) 
                    : (
                        <Fragment>
                            {data.books.map(book => <li key={book.id} onClick={(e) => {this.setState({selected: book.id})}} >{book.name}</li>)}
                        </Fragment>
                    )
                }  
                </ul>
                <BookDetails bookId={this.state.selected}/>
            </div>
        );
    }
};

export default graphql(getBooksQuery)(BookList);