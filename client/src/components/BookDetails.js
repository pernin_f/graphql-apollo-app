import React, { Component, Fragment } from 'react';
import { graphql } from 'react-apollo';
import { getBookQuery } from '../queries/queries';

class BookDetails extends Component {
    displayBookDetails() {
        const { book } = this.props.data;

        if(book) {
            return(
                <Fragment>
                    <h2>{book.name}</h2>
                    <p>{book.genre}</p>
                    <p>{book.author.name}</p>
                    <p>All books by this author : </p>
                    <ul>
                        {
                            book.author.books.map(item => <li key={item.id}>{item.name}</li>)
                        }
                    </ul>
                </Fragment>
            )
        } 
        else {
            return (
                <Fragment>
                    <p>Output book details here</p>
                </Fragment>
            );
        }
    }

    render() {
        return (
            <Fragment>
                {this.displayBookDetails()}
            </Fragment>
        );
    }
};

export default graphql(getBookQuery, {
    options: (props) => {
        return {
            variables: {
                id: props.bookId
            }
        }
    }
})(BookDetails);