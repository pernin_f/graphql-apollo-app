import express from 'express';
import cors from 'cors';
import graphqlHTTP from 'express-graphql';
import mongoose from 'mongoose';

import schema from './schema/schema';

const app = express();

mongoose.connect('mongodb://test123:test123@ds155492.mlab.com:55492/graphql-db-test');
mongoose.connection.once('open', () => {
  console.log('connected to database');
});


app.use('/graphql', cors(), graphqlHTTP({
  schema,
  graphiql: true,
}));

app.listen(4000, () => {
  console.log('now listening for requests on port 4000');
});
